
public class Variablen { //Klassenname wird groß geschrieben
	public static void main(String[] args) {

		//Variablendeklaration (Erstellung einer Variablen, Datentyp wird zugewiesen)
		//Variablennamen immer klein schreiben!
		int meinVermoegen;
		
		//Initialisierung der Variablen (Startwert wird zugewiesen)
		meinVermoegen = 20;
		
		//Deklaration und Initialisierung kann auch in einem Schritt erfolgen
		int robisVermoegen = 4000;
		
		/*Berechnung
		Dabei: das “=“ ist ein Zuweisungsoperator: der Wert auf der rechten Seite
		wird der Variablen auf der linken Seite zugewiesen. Also erst erfolgt die
		Berechnung, dann wird das Ergebnis in der Variablen speichern */
		meinVermoegen = meinVermoegen + 100; //neuer Wert von „meinVermoegen“: 120
		
		/*Ausgabe vom Text „meinVermoegen: “ und vom Wert der
		Variablen meinVermoegen, also momentan 120
		Befehl, um Eine Bildschirmausgabe zu erzeugen: System.out.println();  */
		System.out.println("meinVermoegen: " + meinVermoegen);
		
		/*Ausgabe vom Text „robisVermoegen: “ und vom Wert der
		Variablen meinVermoegen, also momentan 4000 */
		System.out.println("robisVermoegen: " + robisVermoegen);
		
		//“Überfall“
		//Rechte Seite vom = : Berechnungen. Ergebnis wird in Variable geschrieben
		meinVermoegen = meinVermoegen + robisVermoegen; //neuer Wert: 120 + 4000
		
		//Robi wurde ausgeraubt, sein Vermögen wird auf 0 gesetzt
		robisVermoegen = 0;
		System.out.println("meinVermoegen: " + meinVermoegen); //4120
		System.out.println("robisVermoegen: " + robisVermoegen); // 0
		
	} // end of main
} // end of class variablen
