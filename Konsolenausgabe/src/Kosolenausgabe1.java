
public class Kosolenausgabe1 {

	public static void main(String[] args) {
		
		System.out.println("Das ist ein \"Beispielsatz\". ");
		System.out.print("Und ein zweiter. (nicht mehr) In der selben Zeile\n\n\n");

		//print erzeugt eine Augabezeile
		//println eine Ausgabezeile und einen Zeilenumbruch 
		
		
		//Aufgabe 2
		System.out.println("Aufgabe2");
		String s = "*";
		System.out.printf("%6s\n", s);
		System.out.printf("%7s\n", s+s+s);
		System.out.printf("%8s\n", s+s+s+s+s);
		System.out.printf("%8s\n", s+s+s+s+s+s+s);
		System.out.printf("%8s\n", s+s+s+s+s+s+s+s+s);
		System.out.printf("%8s\n", s+s+s+s+s+s+s+s+s+s+s);
		System.out.printf("%7s\n", s+s+s);
		System.out.printf("%7s\n\n\n\n\n", s+s+s);
		
		//Aufgabe 3
		System.out.println("Aufgabe3");
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 100000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e); 
		
	}
}
